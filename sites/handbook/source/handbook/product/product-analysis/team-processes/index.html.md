---
layout: handbook-page-toc
title: Team Processes
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Gearing Ratios

At GitLab, we use gearing ratios as [Business Drivers](/handbook/finance/financial-planning-and-analysis/#business-drivers-also-known-as-gearing-ratios) 
to forecast long term financial goals by function. The Product Analysis group currently focuses 
on one gearing ratio: Product Managers per Product Analyst. In the future, we may consider other 
ratios (ex: Active Experiments per Product Analyst), but for the moment we are focussing on the 
PM:Product Analyst ratio.

### Targets

The long-term target for the Product Managers per Product Analyst ratio is 3:1. The ability of 
PMs to self-serve on day-to-day questions and activities is a critical component to finding 
success at this ratio, and finding the best tool is a focus of the R&D Fusion Team in FY22 Q3-Q4. 
In addition, we want to ensure that analysts are not spending more time context switching 
(changing from one unrelated task to another) and learning the nuances of different data sets 
than they are actually conducting analysis. We want our product analysts to spend their time 
answering complex questions, developing or improving metrics, and making business-critical 
recommendations.

In order to validate our target ratio, we looked at the practices of other large product 
organizations, including Linkedin, Intuit, HubSpot, Squarespace, iHeartRadio, and Peloton 
Digital. We found that most maintained a ratio of 1.5-3 PMs per product analyst, in addition to 
a self-service tool. As such, we feel comfortable setting a target of 3 PM:1 Product Analyst ratio.

### Closing the Gap

The current PM:Product Analyst ratio is ~10:1 - 40 IC product managers (including current openings) 
and 4 product analysts (3 ICs and 1 IC/Manager hybrid). We plan to hire 4 more analysts by 
the end of 2022, which would bring the ratio to 5:1 (assuming the PM head count remains the same). 
As we work to close the gap and move towards to the 3:1 target, we encourage PMs to leverage 
[office hours](/handbook/product/product-analysis/#office-hours).
