---
layout: markdown_page
title: "Lauren Barker's README"
description: "Learn more about working with Lauren Barker"
---

## Lauren Barker README

Fullstack Engineer, Marketing

## Related pages
* [Team Page](https://about.gitlab.com/company/team/#laurenbarker)
* [gitlab.com/laurenbarker](https://gitlab.com/laurenbarker)
* [linkedin.com/in/weblbdesigns/](https://www.linkedin.com/in/weblbdesigns/)

## Current work

[Lauren is currently working on these projects](https://bit.ly/2ZmrkgF).

## About Me

- 🤙 I'm born and raised on the Central Coast of California.
- 🏄 Surfing and the ocean is where I feel at home.
- ❤️ I love design and development.
- 📖 I'm a lifelong learner who values knowledge.
- 🍸 Coffee, beer, or wine please.

### Books 

These are books that have made an impact on me along with a great book on baking bread =)

- The Importance of Living, Lin Yutang
- The Prophet, Kahlil Gibran
- Beard on Bread, James Beard

### Myers-Briggs Personality type:

🔗[**“The Protagonist”** (ENFJ-A/ENFJ-T)](https://www.16personalities.com/articles/assertive-protagonist-enfj-a-vs-turbulent-protagonist-enfj-t)

**Individual traits:**

Mind: **72% Extraverted**

Energy: **53% Intuitive**

Nature: **51% Feeling**

Tactics: **54% Judging**

Identity: **57% Assertive**


## Conclusion

Life is short, make it a blast!