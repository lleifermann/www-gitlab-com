title: What is a GitOps workflow?
seo_title: What is a GitOps workflow?
description: What is a GitOps workflow?
header_body: Managing IT infrastructure can be challenging, but teams that use
  well-known software development practices, including version control, code
  review, and CI/CD pipelines, find the process more convenient. By using config
  files, the same infrastructure environment is deployed each time. Many teams
  know that this workflow increases efficiency, collaboration, and stability,
  but they may wonder what it means to adopt GitOps.
canonical_path: /topics/gitops/gitops-workflow/
file_name: gitops-workflow
parent_topic: gitops
twitter_image: /images/opengraph/iac-gitops.png
related_content:
  - url: https://about.gitlab.com/topics/gitops/
    title: What is GitOps?
cover_image: /images/topics/gitops-header.png
body: >-
  # Three components of GitOps workflows


  As a software development framework, GitOps has three main parts to its workflow, including infrastructure as code, merge requests, and CI/CD pipelines.


  ## 1. Infrastructure as code (IaC)


  The first step in a GitOps workflow is defining all infrastructure as code. IaC automates the IT infrastructure provisioning by using configuration files. IaC is a DevOps practice that supports teams to version infrastructure to improve consistency across machines to reduce deployment friction. Infrastructure code undergoes a similar process as application code with touchpoints in continuous integration, version control, testing, and continuous deployment. Automation leads to more [efficient](https://about.gitlab.com/blog/2020/04/17/why-gitops-should-be-workflow-of-choice/) development, increased consistency, and [faster](https://about.gitlab.com/blog/2021/02/24/production-grade-infra-devsecops-with-five-minute-production/) time to market.


  Managing [infrastructure](https://about.gitlab.com/blog/2020/11/09/lessons-in-iteration-from-new-infrastructure-team/) has traditionally been a manual process involving large teams maintaining physical servers. Each machine often has its own configuration, leading to snowflake environments. With infrastructure as code, teams have increased visibility, consistency, stability, and scalability. 


  ## 2. Merge requests (MRs)


  Declarative tools, such as Kubernetes, enable config files to be [version controlled](https://about.gitlab.com/blog/2020/11/12/migrating-your-version-control-to-git/) by Git, an open source version control system that tracks code changes. Using a Git repository as the single source of truth for infrastructure definitions, GitOps benefits from a robust audit trail. The second aspect of GitOps workflows involve merge requests, which serve as the [change function](https://about.gitlab.com/blog/2020/10/13/merge-request-reviewers/) for infrastructure updates.


  Teams collaborate in merge requests through [code reviews](https://about.gitlab.com/blog/2021/01/25/mr-reviews-with-vs-code/), comments, and suggestions. A merge commits to the [main](https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/) branch and acts as an audit log. Built-in rollback features enable teams to revert to a desired state and explore innovative ways to approach difficult challenges. Merge requests facilitate experimentation and provide a safe way for team members to receive fast [feedback](https://about.gitlab.com/blog/2021/03/18/iteration-and-code-review/) from their peers and subject matter experts. 


  ## 3. Continuous integration and continuous deployment (CI/CD)


  GitOps automates infrastructure management using a Git workflow with [effective](https://about.gitlab.com/blog/2020/07/29/effective-ci-cd-pipelines/) continuous integration and continuous deployment. After code is merged to the main branch, the CI/CD pipeline initiates the [change](https://about.gitlab.com/blog/2021/02/22/pipeline-editor-overview/) in the environment. Manual changes and human error can cause configuration drift and snowflake environments, but GitOps automation and continuous deployment overwrites them so the environment always [deploys](https://about.gitlab.com/blog/2021/02/05/ci-deployment-and-environments/) a consistent desired state.
resources:
  - url: https://about.gitlab.com/solutions/gitops/
    type: Articles
    title: Discover how GitLab supports GitOps workflows
  - title: Hear about the future of infrastructure automation
    url: https://about.gitlab.com/why/gitops-infrastructure-automation/
    type: Webcast
  - url: https://about.gitlab.com/blog/2021/04/27/gitops-done-3-ways/
    title: Learn three ways to approach GitOps
    type: Blog
